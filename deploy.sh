#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

himage dwikiorg mkdir -p /etc/named
hcp wiki.org/named.conf dwikiorg:/etc/.
hcp wiki.org/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 


## configuration de iut.re 
# configuration du serveur dns

himage diutre mkdir -p /etc/named
hcp iut.re/named.conf diutre:/etc/.
hcp iut.re/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 


## configuration de pc1
# resolv.conf

hcp pc1/resolv.conf pc1:/etc/.

## configuration de dorg
# configuration du serveur dns  

himage dorg mkdir -p /etc/named
hcp org/named.conf	dorg:/etc/.
hcp org/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf 

## configuration de dre
#serveur dns 

himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf

## configuration de rt.iut.rt
# serveur dns

himage drtiutre mkdir -p /etc/named
hcp rt.iut.re/named.conf drtiutre:/etc/.
hcp rt.iut.re/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf

## configuration pc2
# resolv.conf

hcp pc2/resolv.conf pc2:/etc/.


